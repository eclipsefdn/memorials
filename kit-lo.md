# In Memory of Kit Lo

Kit Lo, Technical Lead for IBM Eclipse SDK, based out of IBM RTP lab, passed
away on Saturday, Feb 26th, 2022. He was on a routine run on Saturday and
collapsed on the way. Kit has been with IBM for 32+ years, working in various
roles and different teams. In the past decade, Kit was the lead for Translation
/ Globalization efforts across various products. About 5 years ago, Kit took up
the IES Technical Lead role. Kit was a very hard worker, continuous learner and
always willing to go the extra mile to help those in need. Kit was also very
active in Eclipse Open Source Community. He was the co-lead for Eclipse Babel
project (community driven translation of strings in Eclipse distribution) and
in the past year, he also took up the additional role of Eclipse IDE Release
Engineer.

Please add your contribution below via a merge request. 

## Jonah Graham

I have had the honour of working downstream of Kit in the Eclipse IDE release
engineering for the last year. I appreciate all that he did for the community
over the years.

## Denis Roy

Kit was one of the kindest people I'd ever met. We've been working on the Babel
translation project for almost 15 years. He was an involved community player who
fostered contribution, and worked tirelessly, with the little available time he
had, to ensure everything was done right. He fully understood the importance
of globalization and was always eager to help.

Thank you for everything, Kit. You are missed already.

## Chuck Bridgham

I haven't been active @ Eclipse for some years, but as an early IBM contributor - I used to interact with Kit on a regular basis.
He was always helpful, with no drama.  When he asked of your assistance ( Usually because I screwed up a release) He was always humble, and kind.
He was crucial in keeping IBM's Eclipse participation organized and steady.
He was someone who understood the value of Open Source, and the collaboration it encouraged.
I feel deeply sorry for his family, and hope they understand the impact Kit had on the Eclipse Community.

Thank you Kit.
