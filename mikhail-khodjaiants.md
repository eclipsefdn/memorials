# In Memory of Mikhail Khodjaiants

Mikhail Khodjaiants, a long time committer and substantial contributor to the CDT project over the years
passed away in May 2019. Mikhail was a key architect and maintainer of initially the CDI (C Debugger
Interface for Eclipse CDT) and later contributed substantially to its replacement, DSF. Mikhail
started contributing to CDT in its earliest days with the first recorded commit in 2002. Please
visit his [virtual tribute site](https://www.tubmanfuneralhomes.com/notices/Mikhail-Khodjaiants).

Please add your contribution below via a merge request. 

## Jonah Graham

Mikhail presence at CDT calls and ongoing contribution to our community will be missed. Mikhail
helped me on many occasions navigate the complexites of DSF, especially around breakpoint
handling.
